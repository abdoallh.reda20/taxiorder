<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDelegaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delegares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('phone');
            $table->string('password');
            $table->string('name_bank');
            $table->integer('account_number');
            $table->text('type_car');
            $table->string('img_id');
            $table->integer('number_id');
            $table->string('img_car');
            $table->string('img_drive_license');
            $table->string('img_car_license');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delegares');
    }
}
