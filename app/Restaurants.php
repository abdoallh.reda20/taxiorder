<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurants extends Model
{
    //
    public function type()
    {
        return $this->belongsTo('App\RequestsType');
    }
}
